// Задание
// Реализовать переключение вкладок (табы) на чистом Javascript.
//
//     Технические требования:
//
//     В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки.
//     При этом остальной текст должен быть скрыт.
//     В комментариях указано, какой текст должен отображаться для какой вкладки.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//     Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
//     При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.



const elementsArray = [...document.querySelectorAll(".tabs-title")];
const contentArray = [...document.querySelectorAll(".tabs-content li")];

document.querySelector(".tabs").onclick = (event) => {
    removeActive()
        event.target.classList.add("active");
        document.querySelector(`.tabs-content li[data-tab="${event.target.dataset.tab}"]`).classList.add("active");
};

function removeActive() {
    const clsActive = [...document.querySelectorAll('.active')];
    return clsActive.map((el) => {
        return(el.classList.remove('active'))
    });
}

elementsArray.forEach((el, i) => {
    el.dataset.tab = el.textContent;
    contentArray[i].dataset.tab = el.textContent;
    elementsArray[i].dataset.tab = elementsArray[i].textContent;
});
